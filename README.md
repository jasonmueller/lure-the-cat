# Lure-The-Cat

Jason Müller (muellerj@tropos.de)

May 9, 2024

StuMeTa 2024 in Leipzig

---

## How to play

### The scenario
* All teams are all sitting in an empty room with one door 
* Each team gets a specific amount of treats
* A known amount of  very cute cats are released into the room one by one
* ***The cats are always hungry ans will go to the team that offers them the most treats***
* ***The teams are in desperate need of some cat-cuddeling and want to lure in the most cats***
* A team cannot place more treats than it currently has

### Aim of the game 
***Pet more cats than all other teams***

### Gameplay
1. Before a cat enters the room, each team simultaneously places a specific number of treats as a bet
2. The cat enters the room and will join the team with the largest amount of treats 
3. If there is a tie, the cat will decide among the winners
4. All placed treats of all teams will be tossed out
5. The teams place a new number of treats for the next cat
6. Repeat


## Some hints 
* You loose every treat you bet wether you win this round or not
* You don't have to bet each round if you don't want to 
* In an ideal world you would only slightly outbid the second highest bid
* You can track the other teams budgets 
* You can track the other teams bidding behaviour
* Treats that are unused after the last round are worth nothing


## Important note
* ***This is not a game of luck*** ... There are stronger and less strong solutions to this problem 
* ***There is no perfect stategy*** ... even the strongest algorythm can be countered (I think)
* ***Be creative*** 
* ***Have fun***


## Disclaimer
* This program could have been written in a much nicer way using more complex objects. Please don't take this as an example of very well written code. 


